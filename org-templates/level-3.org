#+include: "./common.org"

#+HTML_LINK_UP: ../index.html
#+HTML_LINK_HOME: ../../../index.html

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../../themes/readtheorg/style/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../../themes/readtheorg/style/css/readtheorg.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../../themes/readtheorg/style/css/extra.css"/>

#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="../../..themes/readtheorg/style/lib/js/jquery.stickytableheaders.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="../../../themes/readtheorg/style/js/readtheorg.js"></script>
